#!/usr/bin/make -f
# Uncomment this to turn on verbose mode.
#export DH_VERBOSE=1
export PYBUILD_NAME=pysvn

export DEB_BUILD_MAINT_OPTIONS = hardening=+all,-pie


DEB_HOST_MULTIARCH ?= $(shell dpkg-architecture -qDEB_HOST_MULTIARCH)

PY3VERS := $(shell py3versions -vr debian/control)
APR_INC	= $(strip $(shell apr-config --includes | sed 's/-I//'))

%:
	dh $@ --with=python3 --buildsystem=pybuild

override_dh_auto_configure:
	set -e && for i in $(PY3VERS); do \
		cd $(CURDIR)/Source; python$$i setup.py configure \
		  --pycxx-src-dir=/usr/share/python$$i/CXX \
	          --pycxx-dir=/usr/include/python$$i \
	          --svn-lib-dir=/usr/lib/$(DEB_HOST_MULTIARCH) \
                  --apr-inc-dir=$(APR_INC) \
                  --apu-inc-dir=$(APR_INC) \
	          --apr-lib-dir=/usr/lib/$(DEB_HOST_MULTIARCH) \
	          --fixed-module-name \
                  --platform=$(shell dpkg-architecture -qDEB_HOST_ARCH_OS) ; \
	    mv $(CURDIR)/Source/Makefile $(CURDIR)/Source/MakefilePy$$i ; \
	done


override_dh_auto_build:
	set -e && for i in $(PY3VERS); do \
		mkdir -p $(CURDIR)/Source/pysvn/python3/python$$i/dist-packages/pysvn ; \
		mv $(CURDIR)/Source/MakefilePy$$i $(CURDIR)/Source/Makefile ; \
		$(MAKE) -C $(CURDIR)/Source ; \
		\
		mv $(CURDIR)/Source/pysvn/__init__.py $(CURDIR)/Source/pysvn/python3/python$$i/dist-packages/pysvn ; \
		mv $(CURDIR)/Source/pysvn/_pysvn*.so $(CURDIR)/Source/pysvn/python3/python$$i/dist-packages/pysvn ; \
		$(MAKE) clean -C $(CURDIR)/Source ; \
	done

override_dh_python3:
	dh_python3
	rm -rf $(CURDIR)/debian/python3-svn/usr/lib/python3.*

override_dh_auto_clean:
	dh_auto_clean
	rm -rf $(CURDIR)/Source/pysvn/python3
	rm -rf $(CURDIR)/debian/python-pysvn-dbg
	rm -rf $(CURDIR)/debian/python3-pysvn
